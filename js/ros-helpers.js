class RosHandler {
  constructor(server_address) {
    this.ros = new ROSLIB.Ros();
    this.ros.on('connection', function() {
      console.log('Connected to websocket server.');
    });
    
    this.ros.on('error', function(error) {
      console.log('Error connecting to websocket server: ', error);
    });
    
    this.ros.on('close', function() {
      console.log('Connection to websocket server closed.');
    });

    this.topics = {};
    this.services = {};

    if (server_address != undefined)
      this.connect(server_address);
  }

  connect(server_address) {
    var a = server_address;
    if (server_address == undefined) {
      a = 'ws://localhost:9090';
    }
    // Create a connection to the rosbridge WebSocket server.
    this.ros.connect(a);
  }

  create_subscriber(topic, type, topic_callback) {
    this.topics[topic] = new ROSLIB.Topic({
      ros: this.ros,
      name: topic,
      messageType: type,
      queue_length: 100
    });
    console.log("connecting to topic " + topic);
    this.topics[topic].subscribe(topic_callback);
  }

  create_publisher(topic, type) {
    let publisher = new ROSLIB.Topic({
      ros: this.ros,
      name: topic,
      messageType: type,
      queue_length: 100
    });
    console.log("connecting to topic " + topic);
    return publisher;
  }

  create_service(name, type) {
    this.services[name] = new ROSLIB.Service({
      ros: this.ros,
      name: name,
      serviceType: type
    });
    console.log("create service " + name);
  }

  call_service(name, values, callback) {
    let request = ROSLIB.ServiceRequest(values);
    this.services[name].callService(request, callback, function (m) {
      console.log("error in service " + name + ": " + d);
    });
  }
}
