const goal_colors = [
    "darkslateblue", // FORMULATED: dark red
    "darkmagenta", // SELECTED: brown red
    "mediumvioletred", // EXPANDED: orange
    "lightsalmon", // COMMITED: blue
    "aquamarine", // DISPATCHED: yellow/mustard
    "black", // EVALUATED: green
    "steelblue", // FINISHED: gray
    "gray"  // DROPPED: dark blue
];

class Actor {

    constructor(name, type, task, lifecycle) {
        this.name = name;
        this.group = new Two.Group();

        this.actor_rect = new Two.RoundedRectangle(0, 0, 250, 120, 10);
        this.actor_rect.linewidth = 3;
        this.actor_rect.opacity = 0.2;
        this.group.add(this.actor_rect);

        let actor_text = new Two.Text(name, -45, -40);
        actor_text.weight = 'bold';
        actor_text.scale = 1.3;
        this.group.add(actor_text);

        let type_text = new Two.Text(type, -45, -10);
        type_text.style = 'italic';
        this.group.add(type_text);

        let task_text = new Two.Text(task, -45, 20);
        task_text.decoration = 'underline';
        this.group.add(task_text);
    
        let lifecycle_img = new Two.Rectangle(75, -10, 90, 90);
        lifecycle_img.stroke = 0;
        lifecycle_img.fill = "url(#lifecycle-" + lifecycle + ")";
        this.group.add(lifecycle_img);

        this.goal_status = new Two.Ellipse(75, 50, 8);
        this.goal_status.stroke = 0;
        this.group.add(this.goal_status);
        this.updateGoal(-1);

        this.children = [];
        this.updateState('unconfigured');
    }

    addChild(actor) {
        this.children.push(actor);
    };

    translate(x, y) {
        this.group.translation.x = x;
        this.group.translation.y = y;
    }

    updateGoal(status) {
        console.log(this.name + " update goal " + status);
        if (status < 0 || status > 5) {
            this.goal_status.opacity = 0;
        }
        else {
            this.goal_status.fill = goal_colors[status];
            this.goal_status.opacity = 1;
        }
    }

    updateState(state) {
        if (this.state == state) {
            console.log("already in state " + state);
            return false;
        } else {
            console.log("changing to state " + state);
            this.state = state;
            if (state == 'unconfigured') {
                this.actor_rect.fill = 'lightgray';
                this.actor_rect.stroke = 'darkslategray';
            }
            else if (state == 'inactive') {
                this.actor_rect.fill = 'mediumvioletred';
                this.actor_rect.stroke = 'darkmagenta';
            }
            else if (state == 'active') {
                this.actor_rect.fill = 'steelblue';
                this.actor_rect.stroke = 'darkblue';
            }
            return true;
        }
    }
}


class Observer {

    constructor(name, data) {
        this.name = name;
        this.group = new Two.Group();

        this.rect = new Two.Rectangle(0, 0, 150, 200);
        this.rect.linewidth = 2;
        this.rect.opacity = 0.2;
        this.group.add(this.rect);

        let text = new Two.Text(name, 0, -80);
        text.weight = 'bold';
        text.scale = 1.3;
        this.group.add(text);

        this.data = data.map(function(element, index) {
            let y = index*20 - 50;
            let text = new Two.Text(element, 0, y);
            text.scale = 1.1;
            text.decoration = 'underline';
            return text;
        });
        this.group.add(this.data);

        //this.in_point = new Two.Vector(125, 0);
        this.out_point = new Two.Vector(75, 100);
        this.group.add(this.out_point);

        this.updateState('unconfigured');
    }

    updateState(state) {
        if (this.state == state) {
            console.log("already in state " + state);
            return false;
        } else {
            console.log("changing to state " + state);
            this.state = state;
            if (state == 'unconfigured') {
                this.rect.fill = 'lightgray';
                this.rect.stroke = 'darkslategray';
            }
            else if (state == 'inactive') {
                this.rect.fill = 'mediumvioletred';
                this.rect.stroke = 'darkmagenta';
            }
            else if (state == 'active') {
                this.rect.fill = 'lightsalmon';
                this.rect.stroke = 'coral';
            }
            return true;
        }
    }

    translate(x, y) {
        this.group.translation.x = x;
        this.group.translation.y = y;
    }

}

function save_architecture(name) {
    //get svg element.
    var svg = d3.select("#architecture-graph").select("svg").node();
    //get svg source.
    var serializer = new XMLSerializer();
    var source = serializer.serializeToString(svg);
    //add name spaces.
    if(!source.match(/^<svg[^>]+xmlns="http\:\/\/www\.w3\.org\/2000\/svg"/)){
        source = source.replace(/^<svg/, '<svg xmlns="http://www.w3.org/2000/svg"');
    }
    if(!source.match(/^<svg[^>]+"http\:\/\/www\.w3\.org\/1999\/xlink"/)){
        source = source.replace(/^<svg/, '<svg xmlns:xlink="http://www.w3.org/1999/xlink"');
    }
    //add xml declaration
    source = '<?xml version="1.0" standalone="no"?>\r\n' + source;
    //convert svg source to URI data scheme.
    var url = "data:image/svg+xml;charset=utf-8,"+encodeURIComponent(source);
    //download svg
    let downloadLink = document.createElement("a");
    downloadLink.href = url;
    downloadLink.download = name;
    document.body.appendChild(downloadLink);
    downloadLink.click();
    document.body.removeChild(downloadLink);
}