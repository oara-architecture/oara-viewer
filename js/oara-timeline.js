class OaraTimeline {

  constructor(scale_domain, scale_range, tooltip_function, unique) {
    this.data = [];
    this.timeline = new TimelinesChart();
    this.scale_domain = scale_domain;
    this.scale_range = scale_range;
    this.start_time = null;
    this.last_time = null;
    this.startX = null;
    this.endX = null;
    this.zoomY = [null, null];
    this.tooltip_function = tooltip_function;
    this.unique = unique;
  }

  update(data=null) {
    if (data !== null) {
      this.data = data;
    }

    this.timeline
      .data(this.data)
      .sortAlpha(true)
      .zoomX([
        (this.startX == null) ? this.start_time : this.startX , 
        (this.endX == null) ? this.last_time : this.endX
      ])
      .zoomY(this.zoomY)
      .overviewDomain([this.start_time, this.last_time]);
  }

  on_zoom(zX, zY) {
    console.log(zX);
    this.startX = (zX == null) ? null : zX[0];
    this.endX = (zX == null) ? null : zX[1];
    this.zoomY = (zY == null) ? null : zY;
  }

  create(dom_element) {
    this.timeline
      .dateMarker(false)
      .zQualitative(true)
      .zColorScale(d3.scale.ordinal()
        .domain(this.scale_domain)
        .range(this.scale_range))
      .enableAnimations(false)
      .onZoom((x, y) => this.on_zoom(x, y))
      .segmentTooltipContent(this.tooltip_function)
      .maxLineHeight(25)
      .leftMargin(200)
      .rightMargin(100)
      .sortAlpha(true)
      //.xTickFormat(n => +n)
      (dom_element);
  }

  group(name) {
    let g = this.data.find(g => g.group == name);
    if (g == undefined) {
      let entry = [];
      if (this.unique) {
        entry.push({
          label: name,
          data: []
        });
      }
      let i = this.data.push({
        group: name,
        data: entry
      })
      return this.data[i-1];
    }
    return g;
  }

  label(group, name) {
    if (this.unique) {
      return group.data[0];
    }

    let l = group.data.find(l => l.label == name);
    if (l == undefined) {
      let i = group.data.push({
        label: name,
        data: []
      })
      l = group.data[i-1];
    }
    return l;
  }

  hide(label, goal_id) {
    label.data
      .filter(s => s.ongoing && s.gid == goal_id)
      .forEach(d => {
        let start = d.timeRange[0];
        console.log("hide at time " + start);
        d.timeRange[1] = start;
        d.ongoing = false;
        console.log(d);
      });
  }

  msg_time(stamp) {
    let d = new Date(stamp.sec * 1000 + stamp.nanosec / 1000000);
    if (this.start_time == null || d < this.start_time) {
      this.start_time = d;
      console.log('start time: ' + this.start_time);
    }
    if (this.last_time == null || d > this.last_time) {
      this.last_time = d;
      console.log('last time: ' + this.last_time);
    }
    return d;
  }

  data_time(t_sec, now=new Date()) {
    let d = (t_sec == null) ? now : new Date(t_sec * 1000);
    if (this.start_time == null || d < this.start_time) {
      this.start_time = d;
      console.log('start time: ' + this.start_time);
    }
    return d;
  }

  ongoing_segment(label) {
    return label.data.find(s => s.ongoing);
  }

  save(name) {
    let svgData = this.timeline.getSvg();
    let preface = '<?xml version="1.0" standalone="no"?>\r\n';
    let svgBlob = new Blob([preface, svgData], {type:"image/svg+xml;charset=utf-8"});
    let svgUrl = URL.createObjectURL(svgBlob);
    let downloadLink = document.createElement("a");
    downloadLink.href = svgUrl;
    downloadLink.download = name;
    document.body.appendChild(downloadLink);
    downloadLink.click();
    document.body.removeChild(downloadLink);
  }

  update_ongoing(g, l, t, ongoing=true) {
    let mygroup = this.group(g);
    let mylabel = this.label(mygroup, l);
    let active_segment = this.ongoing_segment(mylabel);
    if (active_segment !== undefined) {
      if (t == null)
        active_segment.timeRange[1] = this.last_time;
      else
        active_segment.timeRange[1] = t;
      active_segment.ongoing = ongoing;
    }
  }
}

function update_last_status(actor, goal, time) {
  data.forEach(group => {
    if (group.group == actor) {
      group.data.forEach(timeline => {
        if (timeline.label == goal) {
          update_goal_status(timeline.data, actor, goal, time);
        }
      });
    }
  });
}

function add_goal_entry(actor, goal, entry, time) {
  update_last_status(actor, goal, time);
  data.forEach(group => {
    if (group.group == actor) {
      group.data.forEach(timeline => {
        if (timeline.label == goal) {
          timeline.data.forEach(element => element.ongoing = false);
          if (entry == undefined) return;
          timeline.data.push(entry);
        }
      });
    }
  });
}
