class OaraStateTable {
        constructor() {

        }
        
        tabulate(data, element) {
                this.table = element.append('table').attr("class", "tab");
                // create a row for each object in the data
                var rows = this.table.selectAll('tr')
                        .data(data)
                        .enter()
                        .append('tr')
                                .attr("id", d => d.name);
        
                this.table.selectAll('tr')
                        .data(data)
                        .append('th')
                                .text(d => d.name);
        
                // create a cell in each row for each column
                var cells = rows.selectAll('td')
                        .data(d => d.states.sort().map(function (x) {
                                return {name: d.name, value: x};
                        }))
                        .enter()
                        .append('td')
                                .attr("align", "center")
                                .attr("id", d => d.name + '-' + d.value)
                                .attr("class", "inactive")
                                .text(d => d.value);
        
                return this.table;
        }
        
        reset(data) {
                this.table.select('#' + data)
                        .selectAll('td')
                        .attr("class", "inactive");
        }

        highlight(data, state) {
                let row = this.table.selectAll('#' + data)
                        .data([data]);
                
                row.enter().append('tr')
                        .attr("id", data)
                        .append('th')
                                .text(data);

                let cell = row.selectAll('#' + data + "-" + state)
                        .data([{name: data, value: state}]);
                
                cell.enter().append('td')
                        .attr("align", "center")
                        .attr("id", d => d.name + '-' + d.value)
                        .attr("class", "inactive")
                        .text(d => d.value);

                cell.attr("class", "active");
        }
}
