# Install

First, source your ROS2 workspace so that the nodejs binding finds all the useful messages.

```
npm install
```

# Start the OARA Viewer Server

```
npm start
```

# Stop the ROS2 Web Server

```
npm stop
```

# Welcome page

open in a browser the following address:

<p align="center"><a href="http://localhost:3030">
        http://localhost:3030
</a></p>